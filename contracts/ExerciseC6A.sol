pragma solidity ^0.4.25;

contract ExerciseC6A {

    /********************************************************************************************/
    /*                                       DATA VARIABLES                                     */
    /********************************************************************************************/

    uint constant M = 5;
    uint constant N = 3;
    struct Profile {
        bool registered;
        bool admin;
    }
    struct Proposal {
        bool done;
        bool proposal;
        uint vote;
    }

    address private owner;
    mapping(address => Profile) profiles;
    mapping(address => Proposal) proposals;

    uint numberM = 0;
    mapping(bool => uint) numberN;

    bool private operational = true;
    uint private vote = 0;

    event OperationProposed(
        bool mode
    );

    /********************************************************************************************/
    /*                                       EVENT DEFINITIONS                                  */
    /********************************************************************************************/

    // No events

    /**
    * @dev Constructor
    *      The deploying account becomes contractOwner
    */
    constructor() public {
        owner = msg.sender;
    }

    /********************************************************************************************/
    /*                                       FUNCTION MODIFIERS                                 */
    /********************************************************************************************/

    // Modifiers help avoid duplication of code. They are typically used to validate something
    // before a function is allowed to be executed.

    /**
    * @dev Modifier that requires the "ContractOwner" account to be the function caller
    */
    modifier requireContractOwner() {
        require(msg.sender == owner, "Caller is not contract owner");
        _;
    }

    /**
    * @dev Modifier that requires the "operational" boolean variable to be "true"
    *      This is used on all state changing functions to pause the contract in 
    *      the event there is an issue that needs to be fixed
    */
    modifier requireIsOperational() {
        require(operational, "Contract is currently not operational");
        _;  // All modifiers require an "_" which indicates where the function body will be added
    }

    /**
    * @dev Modifier that requires user to be registered
    */
    modifier requireIsRegistered() {
        require(profiles[msg.sender].registered, "User is not registered");
        _;  // All modifiers require an "_" which indicates where the function body will be added
    }

    /**
    * @dev Modifier that requires user to be an admin
    */
    modifier requireIsAdmin() {
        require(profiles[msg.sender].admin, "User is not an admin");
        _;  // All modifiers require an "_" which indicates where the function body will be added
    }

    /**
    * @dev Modifier to check that the same admin hasn't voted
    */
    modifier requireHasntVoted() {
        if (proposals[msg.sender].done) {
            require(proposals[msg.sender].vote <= vote, "You have already voted");
        }
        _;
    }

    /********************************************************************************************/
    /*                                       UTILITY FUNCTIONS                                  */
    /********************************************************************************************/

   /**
    * @dev Check if a user is registered
    *
    * @return A bool that indicates if the user is registered
    */   
    function isUserRegistered(address account) external view returns(bool) {
        require(account != address(0), "'account' must be a valid address.");
        return profiles[account].registered;
    }

    /**
    * @dev Get operating status of contract
    *
    * @return A bool that is the current operating status
    */      
    function isOperational() public view returns(bool) {
        return operational;
    }

    /********************************************************************************************/
    /*                                     SMART CONTRACT FUNCTIONS                             */
    /********************************************************************************************/

    function registerUser(address account, bool admin) external requireIsOperational requireContractOwner {
        require(!profiles[account].registered, "User is already registered.");
        if (admin) {
            require(numberM <= M, "You have reached top admin numbers");
            numberM = numberM + 1;
        }
        profiles[account] = Profile({
            registered: true,
            admin: admin
        });
    }

    /**
    * @dev Sets contract operations on/off
    *
    * When operational mode is disabled, all write transactions except for this one will fail
    */    
    function setOperatingStatus(bool mode) external requireIsRegistered requireIsAdmin requireHasntVoted {
        uint number = numberN[mode];
        number = number + 1;
        numberN[mode] = number;
        proposals[msg.sender] = Proposal({
            done: true,
            proposal: mode,
            vote: vote + 1
        });
        if (number >= N) {
            operational = mode;
            vote = vote + 1;
            numberN[true] = 0;
            numberN[false] = 0;
            emit OperationProposed(mode);
        }
    }

}

